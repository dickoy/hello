package hello

import (
	"fmt"
	"rsc.io/quote"
)

func Hello() string {
	return fmt.Sprintf("v0.1.3 - %s", quote.Hello())
}
